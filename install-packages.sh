#!/bin/bash
cd ~/.vim
mkdir -p pack/plugins/start
cd pack/plugins/start
git clone git@github.com:tpope/vim-sensible.git
git clone git@github.com:plasticboy/vim-markdown.git

cd ~/.vim
mkdir -p pack/themes/start
cd pack/themes/start
git clone git@github.com:morhetz/gruvbox.git
