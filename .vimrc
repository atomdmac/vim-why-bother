set number
set cursorline
set background=dark
colorscheme gruvbox
" Sync Vim clipboard to system clipboard
if has('clipboard')
  if has('unix')
    " The `+` register is used by X-server
    set clipboard=unnamedplus
  else
    set clipboard=unnamed
  endif
else
  echom 'This version of VIM does not have clipboard support'
endif
